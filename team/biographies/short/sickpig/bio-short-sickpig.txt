sickpig
Andrea Suisani
BCHN maintainer and developer

A software developer for almost twenty years, recently transitioned
to what they use to call a full-stack software developer. That means
doing 4 jobs while being payed only for one. On the bright side such
a role gives opportunity to improve skills through a lot of IT
fields like database system administration, system administration,
coding and project management.

Holding a Bachelor degree in statistics, and getting into Bitcoin in
2011, Andrea briefly pool mined with a Radeon GPU, and hasn't looked
back since.
